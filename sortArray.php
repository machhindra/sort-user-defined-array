<?php
    session_start();
    //echo "session ----><pre>";print_r($_SESSION);echo "</pre>";
	if (!isset($_SESSION['username'])) {
		$_SESSION['msg'] = "You must log in first";
		header('location: login.php');
	}

	if (isset($_GET['logout'])) {
		session_destroy();
		unset($_SESSION['username']);
		header("location: login.php");
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Array Sorting</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<div class="header">
		<h2 style="width:300px;">User Defined Array</h2>
        <a href="index.php?logout='1'" style="color:red;float:right;">logout</a></h2>
	</div>
	
    <?php if (!isset($_POST['submitlength']) && !isset($_POST['sortArray'])) :        ?>
        <form method="post" action="sortArray.php">

            <div class="input-group">
                <label>Length Of Array</label>
                <input type="text" name="arraysize" >
            </div>
            <div class="input-group">
                <button type="submit" class="btn" name="submitlength">Submit</button>
            </div>
        </form>
	<?php endif                                     ?>

    <?php if (isset($_POST['sortArray'])) { 
                $arrayToSort = array();
                $length =  $_POST['arraylength']; 
                for($i=0; $i<$length; $i++){
                    $arrayToSort[$i] = $_POST['array_'.$i];
                }              ?>
                <div class="content">
                    <h4>Result<h4>
                </div>
                <div class="content">
                    <pre><?php   echo "Entered ";print_r($arrayToSort);        ?></pre><BR><BR>
                </div>
<?php           array_multisort(array_map('count', $arrayToSort), SORT_DESC, $arrayToSort);                                         ?>
                <div class="content">
                    <pre><?php   echo "Sorted ";print_r($arrayToSort);        ?></pre>
                </div>
	<?php }                                    ?>

    <?php if (isset($_POST['submitlength'])) :  
            $length = $_POST['arraysize'];
            echo "Length---->".$length;      ?>
            <form method="post" action="sortArray.php">
   <?php        for($i=0; $i<$length; $i++) {          ?>
                    <div class="input-group">
                        <label>Array[<?php echo $i; ?>]</label>
                        <input type="text" name="array_<?php echo $i; ?>" >
                    </div> 
                        <input type="hidden" name="arraylength" value="<?php echo $length; ?>">
<?php           }               ?>
                <div class="input-group">
                    <button type="submit" class="btn" name="sortArray" value="sortArray">Sort Array</button>
                </div>
            </form>
<?php      endif                   ?>


</body>
</html>