# Sort User Defined Array

This project is used to sort user defined array. User can input single dimentional or multidimentional Array.

### Pre-Requisite
    1. Xampp / Wampp

### Setup
- Download the Sort User Defined Array from below url,
- ( URL :  git@bitbucket.org:machhindra/sort-user-defined-array.git )
- Open a terminal window in the location Xammp->htdocs or wampp->www and type above command to download source code.
	

# Usage
	1. Download this code and put it into Xammp->htdocs or wampp->www folder.
	2. Exact zip files to sortarray folder
	3. Open phpmyadmin and import regitration script.
	4. Run the code on browser. Url like http://localhost:80/sortarray/

